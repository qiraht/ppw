from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('Hobby/', views.halaman2, name='keren'),
    path('Experience/', views.halaman3, name='pengalaman'),
    path('story8/', views.story8, name='booksearch'),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
]