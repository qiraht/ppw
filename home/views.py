from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import Input_register


# Create your views here.
respons = {}
def index(request):
    return render(request, 'home/index.html')

def halaman2(request):
    return render(request, 'home/halaman2.html')

def halaman3(request):
    return render(request, 'home/halaman3.html')

def story8(request):
    return render(request, 'home/story8.html', respons)

def register(request):
    form = Input_register()
    if request.method == 'POST':
        form = Input_register(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Akun '+ user + ' berhasil dibuat')
            return redirect('home:login')
    respons['Input_register'] = form
    return render(request, 'home/register.html', respons)

def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('home:index')
        else:
            messages.info(request, 'Username atau Password salah')

    return render(request, 'home/login.html')
